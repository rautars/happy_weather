Happy Weather
=======================
A weather mod for Minetest (http://minetest.net/)

Weathers included
-----------------------
* Light Rain (code light_rain)
* Rain (code rain)
* Heavy Rain (code heavy_rain)
* Thunder (code thunder)
* Light Snow (code light_snow)
* Snow (code snow)
* Snowstorm (code snowstorm)

Commands
-----------------------
requires `weather_manager` privilege:

* `start_weather <weather code>` 
* `stop_weather <weather code>` 

Dependencies
-----------------------
* [happy_weather_api](https://github.com/xeranas/happy_weather_api) mod-api (used to manage weathers)
* [skylayer](https://github.com/xeranas/skylayer) mod-api (used to manage sky)
* [lightning](https://github.com/minetest-mods/lightning) mod (used by thunder weather).


License of source code:
-----------------------
MIT

Authors of media files:
-----------------------

xeranas:

  * `happy_weather_heavy_rain_drops.png` - CC-0
  * `happy_weather_light_rain_raindrop_1.png` - CC-0
  * `happy_weather_light_rain_raindrop_2.png` - CC-0
  * `happy_weather_light_rain_raindrop_3.png` - CC-0
  * `happy_weather_light_snow_snowflake_1.png` - CC-0
  * `happy_weather_light_snow_snowflake_2.png` - CC-0
  * `happy_weather_light_snow_snowflake_3.png` - CC-0
  * `happy_weather_snowstorm.png.png` - CC-0

inchadney (http://freesound.org/people/inchadney/):

  * `heavy_rain_drop.ogg` - CC-BY-SA 3.0 (cut from http://freesound.org/people/inchadney/sounds/58835/)

rcproductions54 (http://freesound.org/people/rcproductions54/):

  * `light_rain_drop.ogg` - CC-0 (http://freesound.org/people/rcproductions54/sounds/265045/)

Autistic Lucario

  * `happy_weather_snowstorm.ogg` - CC BY 3.0 (http://freesound.org/people/Autistic%20Lucario/sounds/140054/)

uberhuberman

  * `heavy_rain_drop.ogg` - CC BY 3.0 (https://www.freesound.org/people/uberhuberman/sounds/21189/)
